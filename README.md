# Nationella riktlinjer för BIM och geodata
Nationella Riktlinjer för BIM och geodata utgör samhällsbyggnadssektorns rekommendationer och implementeringsstöd för modellorienterade leveranser i livscykeln av den byggda miljön.

## Förvaltare

DEPRECATED! Detta paket underhålls inte längre. se http://nrb.sbplatform.se