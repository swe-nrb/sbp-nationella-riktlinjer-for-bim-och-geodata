Inledning
=========

Nationella riktlinjer för BIM och geodata utgör en samling av kurerade
rekommendationer och exempeldokument kopplat till modellorienterade leveranser
för den byggda miljön.

Dokumenten är publicerade som "öppen källinformation" vilket innebär att
det står allmänheten fritt att använda, förbättra eller kopiera informationen.

 

För vem?
--------

Samtliga aktörer som är delaktiga i livscykelinformationshantering av den byggda miljön
har nytta av gemensamma riktlinjer. Det gör effektivt nyttjande av informationen
enklare och processer tydligare.

Riktlinjerna är således anpassade för en bred användargrupp med olika behov och
intressen, och olika kunskapsnivåer gällande BIM och geodata.

 

Syfte
-----

Syftet är att rekommendera användare enhetliga definitioner för begrepp, metoder
och processer gällande BIM och geodata.

Syftet är även att ge användare implementeringsstöd i form av digitala verktyg,
praktiska anvisningar och exempel.

 

Scope
-----

Riktlinjerna avser i första hand begrepp, metoder och processer relaterade till
leverans av informationsmängder som är modellorienterade.

Gällande nivån för modellorientering avser riktlinjerna hantering av modeller
och enskilda objekt.

Riktlinjerna avser hela livscykeln av modellorienterade informationsmängder för
hus och anläggningar.

 

Vad ingår?
----------

-   Begrepp i form av en sökbar begreppslista med typiska och begrepp relaterade
    till modellorienterade leveranser.

-   En samling av metoder för modellorienterade leveranser.

-   Olika typer av processer inklusive delprocesser för att beskriva
    livscykeln av informationsmängder.

-   Leveransspecifikationer för modellorienterade leveranser i form av mallar
    och exempel.

 

Instruktioner för användning
----------------------------

Underlaget i riktlinjerna ska vara så pass konkreta så att det i princip direkt
kan användas i praktiken. Till exempel vid upprättande av en
leveransspecifikation eller för anvisningar vid överföring av information mellan
två parter.

Användare kan länka till eller ta en kopia på relevant underlag från
riktlinjerna och vid behov anpassa det enligt aktuella förhållanden.

Se även instruktioner för den [digitala
plattformen](http://swe-nrb.gitlab.io/nrb-sbp-shopfront/philosophy/) som används
och som möjliggör effektiv användning.

 

Riktlinjerna och omvärlden 
---------------------------

Riktlinjerna bygger på en samling, urval och vidareutveckling av
(inter)nationella ramstandarder, tillämpningsanvisningar, rekommendationer,
publikationer från aktörer inom Samhällsbyggnadssektorn.

Det finns en uppsjö av olika skrifter och riktlinjer gör inget anspråk på att
ersätta någon av dessa. Genom riktlinjerna får användare en samlad bild av en
del av tillgängliga och relevanta publikationer, samt en vägvisning vid
tillämpning av dessa.

Riktlinjerna utgår, där möjligt och relevant, från ramstandarder och anslutar nära till samtliga delar inom serien Bygghandlingar 90, särskild nära till del 7 och 8, samt
klassifikation enligt CoClass.

 

Tillgång och förvaltning
------------------------

Riktlinjerna är publicerade som "öppen källinformation" och följer [informationspaketstandarden]().
Dokumenteten lagras publikt och förvaltas av aktiva bidragsgivare (contributors).
Endast dokument som följer de [uppsatta kraven]() kan publiceras som nationella
riktlinjer.

Riktlinjerna innehåller texter, tabeller, bilder eller strukturerad data beroende
på behov. För detaljerad information om respektive riktlinje vänligen följ länk
i länklistan.

För nyttjande, ansvar och ägande av riktlinjerna gäller villkoren i första hand
enligt Formas och i andra hand enligt
[MIT](https://opensource.org/licenses/MIT). För nyttjande av standarder och
övriga anvisningar som Riktlinjer hänvisar till och som ägs och/eller förvaltas
av tredje part hänvisas till gällande villkor för de.
